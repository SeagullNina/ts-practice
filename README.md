# Возвращение скачек

Общая задача как и в https://gitlab.com/nsk-front-school/js-hourse-racing — устроить лошадиные скачки.

Только теперь вам предстоит покрыть свой код типами ✨ 

## Начало работы:

1. Установите зависимости с помощью `npm install`
2. Запустите локальный сервер для разработки: `npm start`
2. Откройте в браузере `http://localhost:1234/`
3. Готово!

## Интерфейс игры

В прошлый раз вы столкнулись с этой задачей когда опыта работы с JS было мало, поэтому мы предложили какой интерфейс должен быть у игры. Можно пойти лёгким путём и повторить аналогичный, а можно попробовать придумать что-то своё.

## Как узнать что типы корректны?

- Когда вы запускаете сервер, то одновременно с ним запускается компилятор тайпскрипта, который будет писать ошибки в консоль.
- VSCode и WebStorm из коробки могут подсвечить ошибки в `.ts` файлах. Если используете что-то другое и подсветки нет, то нужно поискать плагины для нужного редактора.
