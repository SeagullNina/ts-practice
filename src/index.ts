let user: User;
type Finish = {name:string, time:number};
type Wages = {horse:string, wager:number};

class User{
  userWages: Wages[];
  account: number;
  constructor(){
    this.account = 100;
    this.userWages = [];
  }
}

class Horse{
  name: string;
  wage: number;
  whenFinished: number;
  constructor(name: string){
    this.name = name;
    this.wage = 0;
    this.whenFinished = 0;
  }
  run ():Promise<Finish> {
    return new Promise(resolve => {
      const timeRandom:number = Math.floor(500 + Math.random() * 2500);
      setTimeout(() => {
        resolve({
          name: this.name,
          time: timeRandom
        });
      }, timeRandom)
    })
}
}
const horses = [
  new Horse('Первая'),
  new Horse('Вторая'),
  new Horse('Третья')
]


function showHorses()//вывод списка лошадей
{
    for (let i = 0; i < horses.length; i++)
    {
        console.log(horses[i].name);
    }  
}

function showAccount()//счёт игрока
{
    console.log(user.account);
}


function setWager(name:string, wage:number)//сделать ставку на лошадь в следующем забеге
{
    try{
        user.account -= wage;
        user.userWages.push({horse: name, wager: wage});
        if (user.account < 0){
            user.account += wage;
            throw new Error('У вас недостаточно средств!');
        }
    }
    catch(err){
        console.log(err.message);
    }
    finally{
    for (let i = 0; i < horses.length; i++)
    {
        if (horses[i].name === name)
        {
            horses[i].name = name;
            horses[i].wage = wage;
        }
    }  
    }
}

function startRacing():void {
  let winner:string;
  const races: Promise<Finish>[] = [];
  horses.forEach((horse) =>{
    races.push(horse.run())
  });
  Promise.race(races)
      .then(winnerIs => {
        winner = winnerIs.name;
      }).catch(() => {
    console.log('Ошибка!');
  });
  Promise.all(races)
      .then(values => {
        values.forEach(horse => {
          console.log(`${horse.name} прошла за ${horse.time}`);
        });
        setWinner();
      })
      .catch(() => {
        console.log('Ошибка!');
      });

    function setWinner():void {
    let accountNew:number = 0;
    console.log(`Победитель - ${winner}`);
    for (let horse of user.userWages) {
      if (horse.horse === winner) {
        accountNew = horse.wager*2;
        user.account += accountNew;
      }
    }
    user.userWages= [];
  }
}

function newGame()//новая игра
{
  user = new User();
}

showHorses();
newGame();
setWager('Первая', 40);
setWager('Вторая', 30);
setWager('Третья', 10);
startRacing();
showAccount();
